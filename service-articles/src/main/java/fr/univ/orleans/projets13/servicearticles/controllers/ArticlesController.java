package fr.univ.orleans.projets13.servicearticles.controllers;

import fr.univ.orleans.projets13.servicearticles.entities.Articles;
import fr.univ.orleans.projets13.servicearticles.exceptions.ArticlesNotFoundException;
import fr.univ.orleans.projets13.servicearticles.services.ServiceArticles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiarticles")
public class ArticlesController {

    @Autowired
    public ServiceArticles serviceArticles;

    @PostMapping(value = "/addarticle")
    public ResponseEntity<Articles> creerArticle(@RequestBody Articles article){
        try {
            //serviceArticles.addArticle(article);
            Articles art = serviceArticles.addArticle(article);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest().path("/{id}")
                    .buildAndExpand(art).toUri();
            return ResponseEntity.created(location).body(article);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    //Liste des articles
    @GetMapping(value = "/Listearticles")
    public ResponseEntity<Iterable<Articles>> getAllArticles() {

        Iterable<Articles> listeArticles = serviceArticles.getAllArticles();
        return ResponseEntity.ok().body(listeArticles);
    }

    // Recherche par son id
    @GetMapping(value = "/searchById/{id}")
    public ResponseEntity<Articles> searchById(@PathVariable("id") String id) {

        Optional<Articles> article = serviceArticles.searchById(id);
        if (article.isPresent()) {
            return ResponseEntity.ok().body(article.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Recherche par Nom
    @GetMapping(value = "/searchByNom/{nom}")
    public ResponseEntity<List<Articles>> getSearchByNom(@PathVariable("nom") String nom) throws ArticlesNotFoundException {
        List<Articles> art = serviceArticles.searchByName(nom);
        if (art.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(art);

    }


    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable("id") String id) {
        try {
            serviceArticles.deleteArticle(id);
        } catch (ArticlesNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.toString());
        }
        return ResponseEntity.ok().build();

        /*Optional<Articles> articles = serviceArticles.searchById(id);
        if(articles.isPresent()){
            serviceArticles.deleteArticle(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();*/
    }
    @PostMapping(value = "/update")
    public ResponseEntity<Articles> updateArticle(@RequestBody Articles articles) {
        Optional<Articles> art = serviceArticles.searchById(articles.getId());
        if (art.isPresent()) {
            if (articles.getCommentaire() == null) {
                articles.setCommentaire(art.get().getCommentaire());
            }
            if (articles.getPrix() == 0) {
                articles.setPrix(art.get().getPrix());
            }
            if (articles.getNom() == null) {
                articles.setNom(art.get().getNom());
            }
            if (articles.getQuantite() == 0) {
                articles.setQuantite(art.get().getQuantite());
            }
            serviceArticles.updateArticles(articles);
            return ResponseEntity.ok().body(articles);
        }
        return ResponseEntity.noContent().build();

    }
}
