package fr.univ.orleans.projets13.servicearticles.exceptions;

public class ArticleSupprimerException extends Exception {
    public ArticleSupprimerException() {
        super("Votre article a été bien suprimmer");
    }
}
