package fr.univ.orleans.projets13.servicearticles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ServiceArticlesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceArticlesApplication.class, args);
    }

}
