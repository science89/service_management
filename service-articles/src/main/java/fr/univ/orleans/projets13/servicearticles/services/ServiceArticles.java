package fr.univ.orleans.projets13.servicearticles.services;

import fr.univ.orleans.projets13.servicearticles.entities.Articles;
import fr.univ.orleans.projets13.servicearticles.exceptions.ArticlesNotFoundException;
import fr.univ.orleans.projets13.servicearticles.repository.ArticlesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceArticles {

    @Autowired
    public ArticlesRepository repos;

    public Articles addArticle(Articles articles) {
        return repos.save(articles);
    }

    // Permet de rechercher les articles par Id
    public Optional<Articles> searchById(String id) {
        return repos.findById(id);
    }

    public List<Articles> searchByName(String nom) {
        return repos.findByNom(nom);
    }

    public Iterable<Articles> getAllArticles() {
        return repos.findAll();
    }

    public void deleteArticle(String id) throws ArticlesNotFoundException {
        //Optional<Articles> articles = repo.deleteById(id);
        if (repos.existsById(id)) {
            repos.deleteById(id);
        } else {
            throw new ArticlesNotFoundException();
        }


        //repo.deleteById(id);
    }

    public Articles updateArticles(Articles articles) {
        return repos.save(articles);
    }
}
