package fr.univ.orleans.projets13.servicearticles.repository;

import fr.univ.orleans.projets13.servicearticles.entities.Articles;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//@RepositoryRestResource(collectionResourceRel = "articles", path = "articles")
@Repository
public interface ArticlesRepository extends MongoRepository<Articles, String> {
    List<Articles> findByNom(@Param("nom") String nom);
}
