package fr.univ.orleans.projets13.servicearticles.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document(collection = "Article")
public class Articles {
    @Id
    private String id;
    private String nom;
    private String description;
    private double prix;
    private int quantite;
    //@DBRef(lazy = true, db = "commentaire")
    private Set<Commentaires> commentaire;

    public Articles() { }

    public Articles(String id, String nom, String description, double prix, int quantite, Set<Commentaires> commentaire) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.quantite = quantite;
        this.commentaire = commentaire;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Set<Commentaires> getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(Set<Commentaires> commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", prix=" + prix +
                ", quantite=" + quantite +
                ", commentaire=" + commentaire +
                '}';
    }
}
