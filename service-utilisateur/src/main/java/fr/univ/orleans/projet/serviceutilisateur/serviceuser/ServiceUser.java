package fr.univ.orleans.projet.serviceutilisateur.serviceuser;


import fr.univ.orleans.projet.serviceutilisateur.interfaceuser.UserRepository;
import fr.univ.orleans.projet.serviceutilisateur.model.Utilisateurs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceUser {

    @Autowired
    private UserRepository repo;

    public Utilisateurs create(Utilisateurs utilisateurs){

        return repo.save(utilisateurs);
    }

    public List<Utilisateurs> getAllUser(){

        return repo.findAll();
    }

    public Optional<Utilisateurs> UserById(Long id){
        Optional<Utilisateurs> user=repo.findById(id);
        if (user.isPresent()){
            return user;
        }
        return Optional.empty();
    }

    public Utilisateurs UserByLogin(String login){

        return repo.findByUsername(login);
    }

    public Utilisateurs getUpdateUser(Utilisateurs utilisateurs){


        return repo.save(utilisateurs);
    }

    public void deleteUser(Long id){

        repo.deleteById(id);
    }




}
