package fr.univ.orleans.projet.serviceutilisateur.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "roles")
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;
    private String name;
    /*@ManyToMany
    private Set<Utilisateurs> utilisateurs= new HashSet<>();*/

    public Roles(String name) {
        this.name=name;
    }

    public Roles(){

    }


    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
