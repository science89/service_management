package fr.univ.orleans.projet.serviceutilisateur.config;

import fr.univ.orleans.projet.serviceutilisateur.model.Utilisateurs;
import fr.univ.orleans.projet.serviceutilisateur.serviceuser.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;

public class CustomUserDetailsService implements UserDetailsService {
    private static final String[] ROLES_USER= {"ROLE_USER"};

    @Autowired
    private ServiceUser serviceUser;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Utilisateurs utilisateurs= serviceUser.UserByLogin(s);
        System.out.println(utilisateurs);
        if(utilisateurs!= null) {
            String[] roles= ROLES_USER;
            UserDetails userDetails= User.builder()
                    .username(utilisateurs.getUsername())
                    .password(passwordEncoder.encode(utilisateurs.getPassword()))
                    .roles( utilisateurs.getRoles().stream().findFirst().get().getName())
                    .build();

            return userDetails;
        } else {
            throw new UsernameNotFoundException("username not found"+s);
        }
    }
}
