package fr.univ.orleans.projet.serviceutilisateur;

import fr.univ.orleans.projet.serviceutilisateur.model.Roles;
import fr.univ.orleans.projet.serviceutilisateur.model.Utilisateurs;
import fr.univ.orleans.projet.serviceutilisateur.serviceuser.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Set;

@SpringBootApplication
public class ServiceUtilisateurApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceUtilisateurApplication.class, args);
    }

     @Autowired
     private ServiceUser serviceUser;

   /* @Autowired
   private PasswordEncoder passwordEncoder;

    @Bean
    public CommandLineRunner init() {
        return args -> {
            // init database with 2 Users
            serviceUser.create(new Utilisateurs("toure","mamady","krm@gmail.com","mady", passwordEncoder.encode("mady"), Set.of(new Roles("user"))) );
            serviceUser.create(new Utilisateurs("kemoko","fabia","fabia@gmail.com","krm", passwordEncoder.encode("krm"), Set.of(new Roles("user"),new Roles("admin"))));

};
    }*/
}
