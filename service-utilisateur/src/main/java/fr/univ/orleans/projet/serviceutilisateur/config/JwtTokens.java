package fr.univ.orleans.projet.serviceutilisateur.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.stream.Collectors;
@Component
public class JwtTokens {


    public static final long EXPIRATION_TIME = 1_000_000;

    @Autowired
    private Key secretkey;

    public String genererToken(UserDetails user) {
        String login = user.getUsername();

        Claims claims = Jwts.claims().setSubject(login);
        var roles = user.getAuthorities().stream().map(auth -> auth.getAuthority()).collect(Collectors.toList());
        claims.put("roles", roles);
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(secretkey)
                .compact();

        return token;

    }
}
