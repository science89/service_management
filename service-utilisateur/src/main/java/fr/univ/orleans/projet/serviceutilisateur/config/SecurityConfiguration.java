package fr.univ.orleans.projet.serviceutilisateur.config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.crypto.SecretKey;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        return new CustomUserDetailsService();
    }

    @Autowired
    private JwtTokens jwtTokens;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http

              .csrf().disable()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(),jwtTokens))
                .authorizeRequests()
                .antMatchers("/apiuser/login/").permitAll()
                .antMatchers("/apiuser/users/**","/apiuser/adduser/**").permitAll()
                .antMatchers(HttpMethod.GET,"/apiuser/userlogin").hasRole("USER")
                .antMatchers(HttpMethod.PUT,"/apiuser/updateuser").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/apiuser/**").hasRole("ADMIN")
                .anyRequest().authenticated().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecretKey getSecretekey() {
        return Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }
}
