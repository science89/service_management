package fr.univ.orleans.projet.serviceutilisateur.controleur;

import fr.univ.orleans.projet.serviceutilisateur.model.Utilisateurs;
import fr.univ.orleans.projet.serviceutilisateur.serviceuser.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiuser")
public class UserControleur {

    @Autowired
    private ServiceUser serviceUser;

    @PostMapping("/adduser")
    public ResponseEntity<Utilisateurs> cresteUser(@RequestBody Utilisateurs utilisateurs){
        Utilisateurs user=serviceUser.create(utilisateurs);
        /*URI location= ServletUriComponentsBuilder
                .fromCurrentRequest().path("/id")
                .buildAndExpand(user.getId()).toUri();*/
        return ResponseEntity.ok().body(user);
    }
    @GetMapping("/users")
    public ResponseEntity<List<Utilisateurs>> getAllUsers(){

       List<Utilisateurs> user= serviceUser.getAllUser();

        return ResponseEntity.ok().body(user);
    }
    @GetMapping("/userid/{id}")
    public ResponseEntity<Utilisateurs> getUserById(@PathVariable("id") Long id){
        Optional<Utilisateurs> users=serviceUser.UserById(id);
        return ResponseEntity.ok().body(users.get());

    }


    @GetMapping("/userlogin/{login}")
    public ResponseEntity<Utilisateurs> getUserBylogin(@PathVariable("login") String login){
        Utilisateurs user= serviceUser.UserByLogin(login);
        return ResponseEntity.ok().body(user);

    }

    @DeleteMapping("/userdelete/{id}")
    public ResponseEntity<Utilisateurs> deleteUserById(@PathVariable("id") Long id){
         Optional<Utilisateurs> utilisateurs=serviceUser.UserById(id);
        if (utilisateurs.isPresent()){
            serviceUser.deleteUser(id);
           return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updateuser/{id}")
    public ResponseEntity<Utilisateurs> majUser(@PathVariable("id") Long id, @RequestBody Utilisateurs utilisateurs){

        Optional<Utilisateurs> user=serviceUser.UserById(id);

       if (user.isPresent()){
           user.get().setNom((utilisateurs.getNom()));
           user.get().setPrenom(utilisateurs.getPrenom());
           user.get().setEmail(utilisateurs.getEmail());
           user.get().setRoles(utilisateurs.getRoles());
           user.get().setUsername(utilisateurs.getUsername());
           user.get().setPassword(utilisateurs.getPassword());
           serviceUser.getUpdateUser(user.get());
           URI location= ServletUriComponentsBuilder
                   .fromCurrentRequest().path("/id")
                   .buildAndExpand(user.get().getId()).toUri();
           return ResponseEntity.created(location).body(user.get());
       }
        return ResponseEntity.notFound().build();

    }







}
