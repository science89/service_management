package fr.univ.orleans.projet.serviceutilisateur.interfaceuser;
import fr.univ.orleans.projet.serviceutilisateur.model.Utilisateurs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;




@Repository
public interface UserRepository extends JpaRepository<Utilisateurs, Long> {

    @Query("select u  from Utilisateurs u where u.username=:username ")
    Utilisateurs findByUsername(@Param("username") String username);

   Utilisateurs save(Utilisateurs utilisateurs);

  //  Utilisateurs save(Utilisateurs utilisateurs, Roles roles);
}
